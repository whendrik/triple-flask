FROM python:3.6

COPY app /deploy/app

RUN pip install -r /deploy/app/requirements.txt

EXPOSE 8080
EXPOSE 8081
EXPOSE 8082


WORKDIR /deploy/app

ENV FLASK_APP=app.py

CMD ["./run_three_webservers.sh"]