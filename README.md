# Triple Flask



Dockerfile for Flask - DoFa!


This will spawn 3 webservers on 3 different ports


## Build Example with docker

```
docker build . -t dofa
```

## Run Example with docker

```
docker run --rm -p 8080:8080 dofa
```

## Run on Open-Shift

Create the app, optionally use `--name=flask-app`
```
oc new-app https://gitlab.com/whendrik/dofa.git
```

Delete the app with
```
oc delete all --selector app=flask-app
```

Log the build process
```
oc logs -f bc/dofa
```

Check which `CLUSTER_IP` is our server
```
oc get svc
```

Test the app

```
curl CLUSTER_IP:8080
```

Optional - expose the service by creating a route

```
oc expose svc/dofa
```