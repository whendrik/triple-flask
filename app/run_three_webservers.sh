#!/bin/bash

export FLASK_APP=app.py

echo run flask on port 8080,8081,8082
flask run -p 8080 -h 0.0.0.0 &
flask run -p 8081 -h 0.0.0.0 &
flask run -p 8082 -h 0.0.0.0
